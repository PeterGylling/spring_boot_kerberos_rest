# README #

HOWTO get a spring boot based service with embedded tomcat and 
spring security to perform Single Sign On using Kerberos and LDAP / ActiveDirectory
as this is rather complex and extremely difficult to get right if you 
aren't aware of all the required settings.

Demonstration of having a protected page (JSP) and a protected endpoint (REST/JSON) along 
with a public welcome page.

# Credit to Karthikeyan Vaithilingam

Karthikeyan Vaithilingam wrote an excellent blog about how to configure kerberos for localhost
access - The code in this project is almost identical, with minor changes to adapt to Spring Security Java configuration
instead of the old XML based configuration.

# Usefull ressources

- http://docs.spring.io/spring-security-kerberos/docs/current/reference/htmlsingle/
- https://tomcat.apache.org/tomcat-8.5-doc/windows-auth-howto.html
- https://seenukarthi.com/security/2014/08/13/localhost-authentication-spring-kerberos/


### Versions in the sample application ###

* Spring Boot: 2.1.3.RELEASE
* Spring Security: 5.1.4.RELEASE
* Apache Tomcat (embedded): 9.0.16
* Spring Security Kerberos: 1.0.1.RELEASE
* Tested versus Active Directory prior to 2012.

### How do I get set up? ###

* Understand the ActiveDirectory Setup in your organisation
* Generate a new kerberos keytab file (To be done on the AD-server)
* Setup kerberos.conf and application.properties (see test resources and *_template files)
* Requires a working keytab file.
* Test your environment using the KrbTest.java class - Edit it before running it
* Start the application from you IDE by running App.java

### Who do I talk to? ###

* peter dot jorgensen at findwise dot com
