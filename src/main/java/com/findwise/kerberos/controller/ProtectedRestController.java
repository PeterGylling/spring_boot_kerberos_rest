package com.findwise.kerberos.controller;

import com.findwise.kerberos.security.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProtectedRestController {

    UserContext userContext;

    @Autowired
    public ProtectedRestController(UserContext userContext) {
        this.userContext = userContext;
    }

    @RequestMapping(
            value = "/user",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public UserEntity getCurrentUser() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName(this.userContext.getUser());
        userEntity.setRoles(this.userContext.getRoles());
        return userEntity;
    }

}
