package com.findwise.kerberos.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;

@Component
public class UserContext {

    public String getUser() {
        return this.getUserDetails().getUsername();
    }

    public Collection<String> getRoles() {
        Collection<? extends GrantedAuthority> grantedAuthorities = this.getUserDetails().getAuthorities();
        HashSet<String> roleSet = new HashSet<>();
        for (GrantedAuthority grantedAuthority : grantedAuthorities) {
            roleSet.add(grantedAuthority.getAuthority());
        }
        return roleSet;
    }

    private UserDetails getUserDetails() {
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
