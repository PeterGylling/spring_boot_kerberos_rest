package com.findwise.kerberos.security;

import org.springframework.ldap.core.ContextSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthority;
import org.springframework.security.ldap.userdetails.NestedLdapAuthoritiesPopulator;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CustomNestedLdapAuthoritiesPopulator extends NestedLdapAuthoritiesPopulator {

    private String ldapGroupSearchBase;
    private String ldapGroupSearchFilter;

    public CustomNestedLdapAuthoritiesPopulator(ContextSource contextSource, String groupSearchBase) {
        super(contextSource, groupSearchBase);
    }


    public Set<GrantedAuthority> getGroupMembershipRoles(String userDn, String username) {
        Set<Map<String, List<String>>> userGroups = this.getLdapTemplate().searchForMultipleAttributeValues(
                this.ldapGroupSearchBase,
                this.ldapGroupSearchFilter,
                new String[] {userDn, username},
                new String[] {this.getGroupRoleAttribute()});
        Set<GrantedAuthority> ldapGrantedAuthoritiesList = new HashSet<>();
        for( Map<String, List<String>> entry : userGroups) {
            String dn = (String) ( (List) entry.get("spring.security.ldap.dn")).get(0);
            String role = entry.get(this.getGroupRoleAttribute()).get(0);
            if(this.isConvertToUpperCase()) {
                role = role.toUpperCase();
            }
            role = this.getRolePrefix() + role;
            ldapGrantedAuthoritiesList.add(new LdapAuthority(role, dn, entry));
        }

        return ldapGrantedAuthoritiesList;
    }

    public void setLdapGroupSearchBase(String ldapGroupSearchBase) {
        this.ldapGroupSearchBase = ldapGroupSearchBase;
    }

    public void setLdapGroupSearchFilter(String ldapGroupSearchFilter) {
        this.ldapGroupSearchFilter = ldapGroupSearchFilter;
    }
}
